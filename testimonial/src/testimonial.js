import classnames from 'classnames';
import { forwardRef } from '@wordpress/element';
import { RichText } from '@wordpress/block-editor';

export function Testimonial( props, ref ) {
  const {
		className,
    background,
    text,
    name,
    title,
    quote,
    children,
    ...additionalProps
	} = props;

	const classes = classnames( 'contodo', 'testamonial', className);

  return (
    <div 
    	className={ classes }
			style={ {color: text, background: background} }
      >
      <div className={ 'quote-container' }>
        <em>
          <RichText.Content 
            className={ 'quote' }
            tagName="h3" 
            value={ quote } />
        </em>
        <div className={ 'image' }>
          { children }
        </div>
        <h5 className={ 'name' }>
          { name }
        </h5>
        <h5 className={ 'title' }>
          { title }
        </h5>
      </div>
    </div>
  );
}

export default forwardRef( Testimonial );