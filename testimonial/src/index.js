import { registerBlockType } from '@wordpress/blocks';

import { __ } from '@wordpress/i18n';

import { quote as icon } from '@wordpress/icons';

import { edit } from './edit'; 
import { save } from './save'; 


registerBlockType( 'contodo-components/testimonial', {
	title: __( 'Contodo Testimonial', 'contodo-components' ),
	description: __(
		'Testamonial',
		'contodo-components'
	),
	category: 'contodo-components',
	icon: icon,
	supports: {
		html: false,
	},
	attributes: {
		align: {
			type: 'string',
			default: 'full',
		},
		background: {
			type: 'string',
			default: '#444444',
		},
		text: {
			type: 'string',
			default: '#ffffff',
		},
		quote: {
			type: 'array',
			source: 'children',
			selector: '.quote',
		},
		name: {
			type: 'array',
			source: 'children',
			selector: '.name',
		},
		title: {
			type: 'array',
			source: 'children',
			selector: '.title',
		},
		imageWidth: {
			type: 'integer',
			default: 50,
		},
		allowedBlocks:{
			type: 'array',
			default: [ 'core/image' ]
		},
		TEMPLATE: {
			type: 'array',
			default: [
				[ 'core/image', {  } ]
			],
		},
	},
	edit: edit,
	save: save,
	styles: [
		{
			name: 'default',
			label: __('default'),
			isDefault: true
		},
		{
			name: 'clip-circle',
			label: __('portrait circle'),
		},
		{
			name: 'clip-ellipse',
			label: __('portait ellipse')
		},
		{
			name: 'clip-bubble',
			label: __('portrait speech bubble')
		}
	],
	supports: {
		align: true,
		align: ['center', 'wide', 'full'],
	}
} );
