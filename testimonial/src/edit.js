import { __ } from '@wordpress/i18n';

import {
  InnerBlocks,
  InspectorControls,
  RichText,
} from '@wordpress/block-editor';

import { 
  ColorPicker,
  Panel,
  PanelBody,
  PanelRow,
  SelectControl,
} from '@wordpress/components';

export const edit = (props) => {
  const {
      className,
      attributes: {
        background,
        text,
        quote,
        name,
        title,
        allowedBlocks,
        TEMPLATE,
      },
      setAttributes,
    } = props;

    return (
      <>
        <InspectorControls>
          <Panel 
            header={ __( 'Testamonial', 'contodo-components' ) }
            className="contodo-testamonial-panel">
            <PanelBody
                title={ __( 'Background Color', 'contodo-components' ) }
                initialOpen={true}
            >
              <PanelRow>
                <ColorPicker
                  color={ {hex: background} }
                  onChangeComplete={ (color) => { setAttributes( { background: color.hex } ) } }
                  />
              </PanelRow>
            </PanelBody>
            <PanelBody
                title={ __( 'Text Color', 'contodo-components' ) }
                initialOpen={true}
            >
              <PanelRow>
                <ColorPicker
                  color={ {hex: text} }
                  onChangeComplete={ (color) => { setAttributes( { text: color.hex } ) } }
                  />
              </PanelRow>
            </PanelBody>
          </Panel>
        </InspectorControls>
        <div 
          className={ ["contodo", "testamonial", className].join(" ") }
          style={ {color: text, background: background} }
          >
          <div className="quote-container">
            <em>
              <RichText
                tagName="h3"
                className="quote"
                placeholder={ __( 'Write something nice…', 'contodo-components' ) }
                keepPlaceholderOnFocus={ true }
                value={ quote }
                allowedFormats={ [ 'core/bold', 'core/link' ]}
                onChange={  (v) => setAttributes( { quote: v } ) }
              />
            </em>
            <div className="image">
              <InnerBlocks 
                allowedBlocks={ allowedBlocks } 
                template={ TEMPLATE } 
                templateLock={ 'all' } />
            </div>
            <RichText
              className="name"
              tagName="h5"
              placeholder={ __( 'Name…', 'contodo-components' ) }
              value={ name }
              allowedFormats={ [ 'core/bold' ]}
              onChange={  (v) => setAttributes( { name: v } )  }
            />
            <RichText
              className="title"
              tagName="h5"
              multiline={ false }
              placeholder={ __( 'Title…', 'contodo-components' ) }
              value={ title }
              allowedFormats={ [ 'core/bold' ]}
              onChange={  (v) => setAttributes( { title: v } )  }
            />
          </div>
        </div>
      </>
    );
  }