import {
  InnerBlocks,
} from '@wordpress/block-editor';

import { Testimonial } from './testimonial';

export const save = ( props ) => {
	const {
		className,
		attributes: {
      background,
      text,
      quote,
      name,
      title,
		},
	} = props;

	return (
		<Testimonial 
			className={ className }
			name={ name }
			title={ title }
			quote={ quote }
			background={ background }
			text={ text }
		>
			<InnerBlocks.Content />
		</Testimonial>
	);
}
