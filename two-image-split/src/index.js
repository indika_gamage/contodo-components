import { registerBlockType } from '@wordpress/blocks';

import { __ } from '@wordpress/i18n';

import { edit } from './edit'; 

import { InnerBlocks } from '@wordpress/block-editor';



registerBlockType( 'contodo-components/two-image-split', {
	title: __( 'Two Image Split', 'contodo-components' ),
	description: __(
		'Two sections of content, split by a highlight image on mobile.',
		'contodo-components'
	),
	category: 'contodo-components',
	icon: <svg width="600" height="400" version="1.1" viewBox="0 0 158.75 105.83" xmlns="http://www.w3.org/2000/svg"><g transform="translate(0 -191.17)"><rect x="79.914" y="205.86" width="70.826" height="79.379" stroke-width=".26458"/><path d="m14.433 228.85h43.565" fill="none" stroke="#000" stroke-width="4.1372"/><rect x="14.433" y="207.73" width="35.28" height="6.1472" fill="#b3b3b3" stroke-width="3.175"/><rect x="14.433" y="247.02" width="46.238" height="29.132" fill="#b3b3b3" stroke-width="3.175"/><rect x=".26727" y="191.16" width="158.49" height="106.11" fill="none" stroke="#000" stroke-width="3.175"/></g></svg>,
	supports: {
		html: false,
	},
	attributes: {
		background: {
			type: 'string',
			default: '#f9f4f0',
		},
		background1: {
			type: 'string',
			default: '#e2d8d1',
		},
		text: {
			type: 'string',
			default: null,
		},
		alignment: {
			type: 'string',
			default: 'right',
		},
		width: {
			type: 'integer',
			default: 58,
		},
		title: {
			type: 'string',
		},
		subtitle: {
			type: 'string',
		},
		content: {
			type: 'string',
		},
		media: {
			type: 'object',
			default: {
				id: undefined,
			},
		},
		allowedBlocks:{
			type: 'array',
			default: [ 'core/image' ]
		},
		TEMPLATE: {
			type: 'array',
			default: [
				[ 'core/image', {  } ]
			],
		},
	},
	edit: edit,
	save: ( props ) => { return ( <InnerBlocks.Content/> ); },
} );
