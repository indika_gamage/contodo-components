import { __ } from '@wordpress/i18n';

import classnames from 'classnames';

import {
  InnerBlocks,
  InspectorControls,
  RichText,
  URLInputButton,
} from '@wordpress/block-editor';

import { 
  Button,
  ColorPicker,
  Panel,
  PanelBody,
  PanelRow,
  SelectControl,
} from '@wordpress/components';

import { MediaInputSingle } from '../../_library/media-input-single';
import { Img } from '../../_library/image';

export const edit = (props) => {
  const {
      className,
      attributes: {
        background,
        background1,
        text,
        alignment,
        width,
        title,
        subtitle,
        content,
        media,
        allowedBlocks,
        TEMPLATE,
      },
      setAttributes,
    } = props;

    return (
      <>
        <InspectorControls>
          <Panel header={ __( 'Settings', 'contodo-components' ) }>
            <PanelBody
                title={ __( 'Secondary Image', 'contodo-components' ) }
                initialOpen={false}
            >
              <PanelRow>
                <MediaInputSingle 
                  allowedTypes={ ['image'] } 
                  media={ media }
                  attrName="media"
                  setAttributes={ setAttributes } />
              </PanelRow>
            </PanelBody>
            <PanelBody
                title={ __( 'Main Image Width', 'contodo-components' ) }
                initialOpen={false}
            >
              <PanelRow>
                <SelectControl
                  label="Width in percentage"
                  value={ width }
                  className="d-block w-100 mb-2"
                  options={ [
                    { label: '25%', value: 25 },
                    { label: '33%', value: 33 },
                    { label: '42%', value: 42 },
                    { label: '50%', value: 50 },
                    { label: '58%', value: 58 },
                    { label: '67%', value: 67 },
                    { label: '75%', value: 75 },
                  ] }
                  onChange={ ( w ) => { setAttributes( { width: w } ) } }
                  />
              </PanelRow>
            </PanelBody>
            <PanelBody
                title={ __( 'Main Image Alignment', 'contodo-components' ) }
                initialOpen={false}
            >
              <PanelRow>
                <SelectControl
                  label="Image alignment"
                  value={ alignment }
                  className="d-block w-100 mb-2"
                  options={ [
                    { label: 'left', value: 'left' },
                    { label: 'right', value: 'right' },
                  ] }
                  onChange={ ( alignment ) => { setAttributes( { alignment: alignment } ) } }
                  />
              </PanelRow>
            </PanelBody>
            <PanelBody
                title={ __( 'Background Color 1', 'contodo-components' ) }
                initialOpen={false}
            >
              <PanelRow>
                <ColorPicker
                  color={ {hex: background} }
                  onChangeComplete={ (color) => { setAttributes( { background: color.hex } ) } }
                  />
              </PanelRow>
            </PanelBody>
            <PanelBody
                title={ __( 'Background Color 2', 'contodo-components' ) }
                initialOpen={false}
            >
              <PanelRow>
                <ColorPicker
                  color={ {hex: background1} }
                  onChangeComplete={ (color) => { setAttributes( { background1: color.hex } ) } }
                  />
              </PanelRow>
            </PanelBody>
            <PanelBody
                title={ __( 'Text Color', 'contodo-components' ) }
                initialOpen={false}
            >
              <PanelRow>
                <ColorPicker
                  color={ {hex: text} }
                  onChangeComplete={ (color) => { setAttributes( { text: color.hex } ) } }
                  />
              </PanelRow>
            </PanelBody>
          </Panel>
        </InspectorControls>
        <div 
          className={ ["contodo", "two-up", "two-up-split", "image-"+width, "image-"+alignment, className].join(" ") }
          style={ {color: text} }
          >
          <div className={ "image" }>
            <InnerBlocks 
              allowedBlocks={ allowedBlocks } 
              template={ TEMPLATE } 
              templateLock={ 'all' } />
          </div>
          <div className={ classnames("content", "content-top") }>
            <div 
              className="bg-tile"
              style={ { background: background } }></div>
            
            <RichText
              tagName="h5"
              multiline={ false }
              placeholder={ __( 'Subtitle…', 'contodo-components' ) }
              value={ subtitle }
              onChange={  (v) => setAttributes( { subtitle: v } )  }
            />
            <RichText
              tagName="h3"
              placeholder={ __( 'Title…', 'contodo-components' ) }
              value={ title }
              onChange={  (v) => setAttributes( { title: v } )  }
            />
            <RichText
              tagName="div"
              multiline="p"
              className="description"
              placeholder={ __( 'Content…', 'contodo-components' ) }
              value={ content }
              onChange={  (v) => setAttributes( { content: v } )  }
            />
          </div>
          <div className={ classnames("content", "content-bottom") }>
            <div 
              className="bg-tile"
              style={ { background: background1 } }></div>
            <div className={ "image" }>
              { !! media.id && (media.type == 'image') && (
                <figure>
                  <Img media={ media } />
                </figure>
                )}
              { !! media.id && (media.type == 'video') && (
                <figure>
                  <video loop muted autoplay="true">
                    <source src={ media.url } type={ media.mime } />
                  </video>
                </figure>
                )}
            </div>
          </div>
        </div>
      </>
    );
  }