<?php

/**
 * Plugin Name: Contodo Components - Two Up Split with Image
 * Plugin URI: https://gitlab.com/markslab/contodo-components
 * Description: This is a plugin with some common components used by contodo.co to make premium custom websites.
 * Version: 1.0
 * Author: mark.is.at.a.computer@gmail.com
 *
 * @package contodo-components
 */

defined( 'ABSPATH' ) || exit;

/**
 * Load all translations for our plugin from the MO file.
*/
add_action( 'init', 'contodo_components_two_image_split_load_textdomain' );

function contodo_components_two_image_split_load_textdomain() {
	load_plugin_textdomain( 'contodo-components', false, basename( __DIR__ ) . '/languages' );
}

/**
 * Registers all block assets so that they can be enqueued through Gutenberg in
 * the corresponding context.
 *
 * Passes translations to JavaScript.
 */
function contodo_components_two_image_split_register_block() {

	// automatically load dependencies and version
	$asset_file = include( plugin_dir_path( __FILE__ ) . 'build/index.asset.php');

	wp_register_script(
		'contodo-components-two-image-split',
		plugins_url( 'build/index.js', __FILE__ ),
		$asset_file['dependencies'],
		$asset_file['version']
	);

	wp_register_style(
		'contodo-components-two-image-split-editor',
		plugins_url( 'editor.css', __FILE__ ),
		array( 'wp-edit-blocks' ),
		filemtime( plugin_dir_path( __FILE__ ) . 'editor.css' )
	);

	wp_register_style(
		'contodo-components-two-image-split',
		plugins_url( 'style.css', __FILE__ ),
		array( ),
		filemtime( plugin_dir_path( __FILE__ ) . 'style.css' )
	);

	register_block_type( 'contodo-components/two-image-split', array(
		'style' => 'contodo-components-two-image-split',
		'editor_style' => 'contodo-components-two-image-split-editor',
		'editor_script' => 'contodo-components-two-image-split',
		'render_callback' => 'contodo_components_two_image_split_render',
	) );

  if ( function_exists( 'wp_set_script_translations' ) ) {
    /**
     * May be extended to wp_set_script_translations( 'my-handle', 'my-domain',
     * plugin_dir_path( MY_PLUGIN ) . 'languages' ) ). For details see
     * https://make.wordpress.org/core/2018/11/09/new-javascript-i18n-support-in-wordpress/
     */
    wp_set_script_translations( 'contodo-components-two-image-split', 'contodo-components' );
  }

}
add_action( 'init', 'contodo_components_two_image_split_register_block' );

function contodo_components_two_image_split_render( $block_attributes, $content ) {
	
	$secondary_image = (isset($block_attributes['media']['id'])) ? wp_get_attachment_image($block_attributes['media']['id'], [640,480]) : '';
	$description = (isset($block_attributes['content'])) ? $block_attributes['content'] : '';

	$vars = [
    'title' => '',
    'subtitle' => '',
    'background' => '#f9f4f0',
    'background1' => '#e2d8d1',
    'media' => '',
    'text' => '',
    'alignment' => 'right',
    'width' => 58,
	];
	foreach ($vars as $item => $default) {
        $$item = (isset($block_attributes[$item])) ? $block_attributes[$item] : $default;
	}

    $output = sprintf(
		'<div class="contodo two-up two-up-split two-image-split image-%1$s image-%2$s">
			<div class="content content-top">
				<h5 class="subtitle">%3$s</h5>
				<div class="bg-tile" style="background:%4$s"></div>
				<h2 class="title">%5$s</h2>
				<div class="description">%6$s</div>
			</div>
			<div class="image">
				%7$s
			</div>
			<div class="content content-bottom">
				<div class="image">%8$s</div>
				<div class="bg-tile" style="background:%9$s"></div>
			</div>
		</div>',
        $alignment, $width,
        $subtitle,
        $background,
        $title,
        $description,
        $content,
        $secondary_image,
        $background1
	);
	return $output;
}
