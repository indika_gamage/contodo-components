import classnames from 'classnames';

import { RichText } from '@wordpress/block-editor';
import { forwardRef } from '@wordpress/element';

import { Subtitle, Title, Content, CallToAction } from '../../_library/atoms';
import { LinksList } from '../../_library/links';

export function TwoUpSplit( props, ref ) {
  const {
		anchor,
    className,
    background,
    text,
    alignment,
    title,
    subtitle,
    content,
    cta,
    links,
    imageWidth,
    children,
    ...additionalProps
	} = props;

	const classes = classnames( 'contodo', 'two-up', 'two-up-split', className, {
		[`image-${imageWidth}`]: true,
		[`image-${alignment}`]: true,
	} );

  return (
    <div 
    	className={ classes }
      id={ anchor }
			style={ {color: text} }
      >
      <div className={ classnames("content", "content-top") }>
				<Title value={ title } />
        <div 
          className="bg-tile"
          style={ {background: background} }></div>
				<Subtitle value={ subtitle } />
				<Content value={ content } />
      </div>
      <div className={ "image" }>
        { children }
      </div>
      <div className={ classnames("content", "content-bottom") }>
        <CallToAction value={ cta } />
        <div 
          className="bg-tile"
          style={ {background: background} }></div>
        <LinksList
          links={ links }
          />
      </div>
    </div>
  );
}

export default forwardRef( TwoUpSplit );