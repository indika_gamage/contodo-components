/**
 * External dependencies
 */
import React from 'react';
const path = require( 'path' );

/**
 * Internal dependencies
 */
import '../../two-up/style.css';
import '../style.css';
import TwoUpSplit from './two-up-split.js';

export default { 
	title: 'Components/Two Up Split', 
	component: TwoUpSplit,
	argTypes: {
		className: {
			control: { 
				disable: true,
			},
		},
		links: {
			control: { 
				disable: true,
			},
		},
		imageWidth: {
			control: {
				type: 'select',
				options: [25, 33, 42, 50, 58 ,67, 75],
			}
		},
		alignment: {
			control: {
				type: 'select',
				options: ['right', 'left'],
			}
		},
		background: {
			control: {
				type: 'color',
			}
		},
		text: {
			control: {
				type: 'color',
			}
		},
		children: {
			control: {
				type: 'select',
				options: {
					'bag image': 'https://heavyish.com/sites/default/files/styles/rollover_large/public/IGP0917.JPG',
					'people image': 'https://images.pexels.com/photos/3228704/pexels-photo-3228704.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260',
					'camping image': 'https://images.pexels.com/photos/1687845/pexels-photo-1687845.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260',
				},
			}
		}
	},
	args: {
	  subtitle: 'Rest and Relax.',
	  title: 'Smooth',
	  content: 'When you want to leave the daily grind and calm your body and mind. Unwind to feel more grounded and present.',
	  cta: 'Explore Smooth',
	  imageWidth: 50,
	  alignment: 'right',
	  background: '#f9f4f0',
	  text: '--var(dark, #000)',
	  children: 'https://heavyish.com/sites/default/files/styles/rollover_large/public/IGP0917.JPG',
	  links: [
	  	{title: 'Ingredients', url: '/vibes/smooth#ingredients'},
	  	{title: 'Prerolls', url: '/products/prerolls#smooth'},
	  	{title: 'Tablets', url: '/products/tablets#smooth'},
	  ],
	  className: '',
	},
}

const Template = (args) => (
	<TwoUpSplit {...args}>
		<figure>
			<img src={ args.children } alt='example image' />
		</figure>
	</TwoUpSplit>
);

export const Default = Template.bind({});

Default.decorators = [(Story) => <div style={{ margin: '5em' }}><Story/></div>]

