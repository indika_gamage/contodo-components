import { __ } from '@wordpress/i18n';

import {
  InnerBlocks,
  RichText,
  MediaUpload,
} from '@wordpress/block-editor';

export const save = ( props ) => {
	const {
		className,
		attributes: {
			title,
			url,
			absLink,
			mediaURL,
			content,
		},
	} = props;

	return (
		<div className={ ["project-card", "card", className].join(" ") }>
			
			<a href={ absLink } className="project-url image">
				<InnerBlocks.Content />
				<RichText.Content tagName="h2" value={ title }/>
				<RichText.Content tagName="div" className="description" value={ content } />
			</a>

		</div>
	);
}
