import { __ } from '@wordpress/i18n';

import {
  InnerBlocks,
  RichText,
  URLInputButton,
} from '@wordpress/block-editor';

import { Button } from '@wordpress/components';

export const edit = (props) => {
  const {
      className,
      attributes: {
        title,
        url,
        absLink,
        content,
      },
      setAttributes,
    } = props;

    const allowedBlocks = ['core/image'];
    const TEMPLATE = [
      ['core/image', {  } ],
    ];

    const getAbsLink = ( url ) => {
      var base_url = window.location.origin
      return url.substring( base_url.length )
    }
    
    const onChangeTitle = ( value ) => {
      setAttributes( { title: value } );
    };

    const onChangeURL = ( value ) => {
      setAttributes( { url: value, absLink: getAbsLink(value) } );
    };

    const onChangeContent = ( value ) => {
      setAttributes( { content: value } );
    };

    return (
      <div className={ ["project-card", "card", className].join(" ") }>
        
        <div className="project-image">
          <InnerBlocks 
            allowedBlocks={ allowedBlocks } 
            template={ TEMPLATE } 
            templateLock={ 'all' } />
        </div>
        <RichText
          tagName="h2"
          placeholder={ __( 'Enter Project title…', 'contodo-components' ) }
          value={ title }
          onChange={ onChangeTitle }
        />
        <URLInputButton
          className="project-url"
          url={ url }
          placeholder={ __( 'Enter the project page url…', 'contodo-components' ) }
          onChange={ onChangeURL }
        />
        <RichText
          tagName="div"
          multiline="p"
          className="description"
          placeholder={ __( 'Enter the project description…', 'contodo-components' ) }
          value={ content }
          onChange={ onChangeContent }
        />
      </div>
    );
  }