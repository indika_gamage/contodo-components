import { __ } from '@wordpress/i18n';

import { 
	MediaUpload,
	MediaUploadCheck,
} from '@wordpress/block-editor';

import {
	PanelBody,
	Button,
	ResponsiveWrapper,
	Spinner,
} from '@wordpress/components';

import { select } from '@wordpress/data';

export function MediaInputSingle ( props ) {
	const {
		media,
    attrName,
		allowedTypes,
    setAttributes,
    className,
    ...additionalProps
	} = props;

  const isEmpty = ( media ) => {
  	return ((typeof media == 'object') && Object.keys( media ).length === 0 && media.constructor === Object);
  }

  const createAttrsObj = ( ) => {
    var attrs = {};
    attrs[attrName] = { id: undefined };
    return attrs;
  }

  const onRemoveMedia = ( ) => {
    var attrs = createAttrsObj();
    setAttributes( attrs );
  };

  const onChangeMedia = ( m ) => {
  	var attrs = createAttrsObj();
    attrs[attrName] = m;
    setAttributes( attrs );
  };

  return (

    <div className="wp-block-image-selector">
      <MediaUploadCheck fallback={ __('You need permission to upload', 'contodo-components') }>
          <MediaUpload
              title={ __( 'Choose media', 'contodo-components' ) }
              onSelect={  onChangeMedia }
              allowedTypes={ allowedTypes }
              value={ media.id }
              render={ ( { open } ) => (
                <Button
                  className={ ! media.id ? 'editor-post-featured-image__toggle' : 'editor-post-featured-image__preview' }
                  onClick={ open }>
                  { ! media.id && ( __( 'Choose media', 'contodo-components' ) ) }
                  { media.id && media.sizes &&
								    <ResponsiveWrapper
							        naturalWidth={ media.sizes.thumbnail.width }
							        naturalHeight={ media.sizes.thumbnail.height }
								    >
								      <img src={ media.sizes.thumbnail.url } alt={ __( 'Media thumbnail', 'contodo-components' ) } />
								    </ResponsiveWrapper>
									}
                </Button>
              ) }
          />
      </MediaUploadCheck>
      { media.id &&
          <MediaUploadCheck>
            <Button onClick={ onRemoveMedia } isLink isDestructive>
              { __( 'Remove media', 'contodo-components' ) }
            </Button>
          </MediaUploadCheck>
      }
    </div>
  )
}


