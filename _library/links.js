

import { __ } from '@wordpress/i18n';

import {
  RichText,
  URLInputButton,
} from '@wordpress/block-editor';

import { 
  Button,
  IconButton,
} from '@wordpress/components';

export function LinksInput (props) {
	const {
		links,
    setAttributes,
    className,
    ...additionalProps
	} = props;

	const onAddLink = ( value ) => {
    const lanks=[...links]
    lanks.push( { title: '', url: '' })
    setAttributes( { links: lanks } );
  };

  const onRemoveLink = ( index ) => {
    const lanks=[...links]
    lanks.splice( index, 1 )
    setAttributes( { links: lanks } );
  };

  const onChangeLink = ( value, index ) => {
    const lanks=[...links]
    Object.assign( lanks[index], value)
    setAttributes( { links: lanks } );
  };

	const linkFields = links.map( (link, index) => {
    return (
        <div 
        	key={ index } 
        	className="contodo-link"
        	style={ {display: 'grid', gridTemplateColumns: '6fr 1fr 1fr'} }
        	>
          <RichText
              tagName="span"
              className="title"
              multiline={ false }
              placeholder={ __( 'Link title', 'contodo-components' ) }
              value={ link.title }
              onChange={ (title) => onChangeLink( { title: title }, index) }
            />
          <URLInputButton 
            url={ link.url } 
            onChange={ (url) => onChangeLink( { url: url }, index) }
            disableSuggestions={ true }
          />
          <IconButton
            className="remove-link"
            icon="no-alt"
            label="Delete link"
            onClick={ () => onRemoveLink( index ) }
          />
        </div>
      );
  } )

  return (
  	<div>
      <p>If you don't see a link, it's because the url hasn't been entered yet.</p>
      { linkFields }
      <Button
        isDefault
        onClick={ onAddLink.bind( this ) }
      >Add Link</Button>
    </div>
  )
}

export function LinksList ( props ) {
	const {
		links,
    linkStyle,
    className,
    ...additionalProps
	} = props;
	
	const linkDisplay = links.map( (link, index) => {
	  if ( link.url.length > 0 ) {
	    return (
	      <li key={ index }>
	      	<a href={ link.url }>
	      		{ link.title }
	      	</a> 
	      </li>
	    );
	  }
	} )

	if (linkStyle == 'ol') {
		return (
	  	<div className={ className }>	
	  		<ol>
	  			{ linkDisplay }
	  		</ol>
	  	</div>
		)
	}

	return (
		<div className={ className }>
			<ul>
				{ linkDisplay }
			</ul>
		</div>
	)
}
