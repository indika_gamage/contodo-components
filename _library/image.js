import { __ } from '@wordpress/i18n';
import { isBlobURL } from '@wordpress/blob';
import { registerStore } from '@wordpress/data';

import {
	Spinner,
} from '@wordpress/components';

export function Img (props) {
	const {
		media: {
			alt,
			caption,
			filename,
			height,
			id,
			mime,
			name,
			orientation,
			sizes,
			subtype,
			type,
			url,
			width,
		},
    className,
    ...additionalProps
	} = props;



	let defaultedAlt;

	if ( alt ) {
		defaultedAlt = alt;
	} else if ( filename ) {
		defaultedAlt = sprintf(
			/* translators: %s: file name */
			__( 'This image has an empty alt attribute; its file name is %s' ),
			filename
		);
	} else {
		defaultedAlt = __( 'This image has an empty alt attribute' );
	}

	return (
		<>
			<img
				src={ url }
				alt={ defaultedAlt }
				width={ width }
				height={ height }
			/>
			{ isBlobURL( url ) && <Spinner /> }
		</>
	);
}