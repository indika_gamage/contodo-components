import { RichText } from '@wordpress/block-editor';

export const Subtitle = ( props ) => {
	if (props.value && props.value.length > 0) 
		return <RichText.Content tagName="h5" value={ props.value } />
}
export const Title = ( props ) => {
	if (props.value && props.value.length > 0) 
		return <RichText.Content tagName="h2" value={ props.value } />
}
export const Content = ( props ) => {
	if (props.value && props.value.length > 0) 
		return <RichText.Content tagName="div" className="description" value={ props.value } />
}
export const CallToAction = ( props ) => {
	if (props.value && props.value.length > 0) 
		return <RichText.Content tagName="div" className="cta" value={ props.value } />
}
export const Link = ( props ) => {
	if (props.url && props.url.length > 0 && props.text && props.text.length > 0) 
		return (
			<div><a href={ props.url }>{ props.text }</a></div>
		)
}