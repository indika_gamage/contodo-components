/**
 * External dependencies
 */
import { addDecorator } from '@storybook/react';
import { withA11y } from '@storybook/addon-a11y';

/**
 * WordPress dependencies
 */

addDecorator( withA11y );


export const parameters = {
	backgrounds: {
		default: 'light',
		values: [
			{
				name: 'light',
				value: '#fff',
			},
			{
				name: 'dark',
				value: '#555',
			},
			{
				name: 'greyish brown',
				value: '#444',
			},
			{
				name: 'brown grey',
				value: '#707070',
			},
			{
				name: 'grey',
				value: '#a3a3a3',
			},
			{
				name: 'light orange',
				value: '#ffa744',
			},
			{
				name: 'pumpkin orange',
				value: '#ff8700',
			},
			{
				name: 'ash',
				value: '#f9f9f9',
			},
			{
				name: 'turquoise',
				value: '#00becd',
			},
		],
	},
	layout: 'centered',
}
