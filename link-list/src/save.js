import LinkList from './links'; 

export const save = ( props ) => {
	const {
		className,
		attributes: {
      links,
      linkStyle,
		},
	} = props;

	return (
		<LinkList 
			className={ className }
			links={ links }
			linkStyle={ linkStyle }
			/>
	);
}
