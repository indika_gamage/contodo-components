import { __ } from '@wordpress/i18n';

import LinkList from './links'; 

import {
  InspectorControls,
  RichText,
  URLInputButton,
} from '@wordpress/block-editor';

import { 
  Button,
  IconButton,
  PanelBody,
  PanelRow,
  SelectControl,
} from '@wordpress/components';

export const edit = (props) => {
  const {
    className,
    attributes: {
      links,
      linkStyle,
    },
    setAttributes,
  } = props;

  const onAddLink = ( value ) => {
    const lanks=[...links]
    lanks.push( { title: '', url: '' })
    setAttributes( { links: lanks } );
  };

  const onRemoveLink = ( index ) => {
    const lanks=[...links]
    lanks.splice( index, 1 )
    setAttributes( { links: lanks } );
  };

  const onChangeLink = ( value, index ) => {
    const lanks=[...links]
    Object.assign( lanks[index], value)
    setAttributes( { links: lanks } );
  };


  const linkFields = links.map( (link, index) => {
    return (
        <div key={ index } className="contodo-link">
          <RichText
              tagName="span"
              className="title"
              multiline={ false }
              placeholder={ __( 'Link title', 'contodo-components' ) }
              value={ link.title }
              onChange={ (title) => onChangeLink( { title: title }, index) }
            />
          <URLInputButton 
            url={ link.url } 
            onChange={ (url) => onChangeLink( { url: url }, index) }
            disableSuggestions={ true }
          />
          <IconButton
            className="remove-link"
            icon="no-alt"
            label="Delete link"
            onClick={ () => onRemoveLink( index ) }
          />
        </div>
      );
  } )

  return (
    <div className={ ["contodo", className].join(" ") }>
      <InspectorControls key={ props.clientId }>
        <PanelBody
            title={ __( 'Links', 'contodo-components' ) }
            initialOpen={true}
        >
          <PanelRow>
            <div>
              <p>If you don't see a link, it's because the url hasn't been entered yet.</p>
              { linkFields }
              <Button
                isDefault
                onClick={ onAddLink.bind( this ) }
              >Add Link</Button>
            </div>
          </PanelRow>
        </PanelBody>
        <PanelBody
            title={ __( 'List wrapper', 'contodo-components' ) }
            initialOpen={true}
        >
          <PanelRow>
            <SelectControl
              value={ linkStyle }
              options={ [
                { label: 'unordered', value: 'ul' },
                { label: 'ordered', value: 'ol' },
              ] }
              onChange={ ( linkStyle ) => { setAttributes( { linkStyle: linkStyle } ) } }
            />
          </PanelRow>
        </PanelBody>
      </InspectorControls>
      <LinkList 
        className={ className }
        links={ links }
        linkStyle={ linkStyle }
        />
    </div>
  );
}