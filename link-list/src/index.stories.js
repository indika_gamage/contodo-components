/**
 * External dependencies
 */
import React from 'react';
const path = require( 'path' );

/**
 * Internal dependencies
 */
import '../style.css';
import LinkList from './links.js';

export default { 
	title: 'Atoms/Links List', 
	component: LinkList,
	argTypes: {
		linkStyle: {
			control: { 
				type: 'select', 
				options: {
					'unordered list':'ul',
					'ordered list':'ol',
				},
			},
		},
		links: {
			control: {
				disable: true,
			},
		},
		className: {
			control: {
				disable: true,
			},
		},		
	},
	args: {
		links: [ 
				{ title: 'Portfolio', url: 'https://whatisthiscrap.com/' },
				{ title: 'Instagram', url: 'https://instagram.com/roystuff' },
				{ title: 'Linkedin', url: 'https://lankedout.com/nodbody' },
				{ title: 'Medium', url: 'https://medium.com/stuff' },
			],
		linkStyle: 'ul',
		className: 'is-style-inline-block',
	},
}

const Template = (args) => <LinkList {...args} />;

export const InlineBlock = Template.bind({});

export const Block = Template.bind({});
Block.args = {
	className: 'is-style-block',
}

export const GridTwoColumn = Template.bind({});
GridTwoColumn.args = {
	className: 'is-style-grid',
}

export const GridThreeColumn = Template.bind({});
GridThreeColumn.args = {
	className: 'is-style-grid-three',
}
export const GridFourColumn = Template.bind({});
GridFourColumn.args = {
	className: 'is-style-grid-four',
}

export const GridFiveColumn = Template.bind({});
GridFiveColumn.args = {
	className: 'is-style-grid-five',
}

export const Flex = Template.bind({});
Flex.args = {
	className: 'is-style-flex',
}

export const List = Template.bind({});
List.args = {
	className: 'is-style-list',
}
