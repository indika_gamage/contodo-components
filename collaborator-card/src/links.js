import {
  ExternalLink,
} from '@wordpress/components';

export const renderLinks = ( links ) => {
	const linkDisplay = links.map( (link, index) => {
    if ( link.url.length > 0 ) {
      return (
        <ExternalLink key={ index } href={ link.url }>{ link.title }</ExternalLink> 
      );
    }
  } )

  return linkDisplay
}