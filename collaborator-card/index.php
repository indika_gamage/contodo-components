<?php

/**
 * Plugin Name: Contodo Components - Two Up Hero
 * Plugin URI: https://gitlab.com/markslab/contodo-components
 * Description: This is a plugin with some common components for contodo.co.
 * Version: 1.0
 * Author: mark.is.at.a.computer@gmail.com
 *
 * @package contodo-components
 */

defined( 'ABSPATH' ) || exit;

/**
 * Load all translations for our plugin from the MO file.
*/
add_action( 'init', 'contodo_components_collaborator_card_load_textdomain' );

function contodo_components_collaborator_card_load_textdomain() {
	load_plugin_textdomain( 'contodo-components', false, basename( __DIR__ ) . '/languages' );
}

/**
 * Registers all block assets so that they can be enqueued through Gutenberg in
 * the corresponding context.
 *
 * Passes translations to JavaScript.
 */
function contodo_components_collaborator_card_register_block() {

	// automatically load dependencies and version
	$asset_file = include( plugin_dir_path( __FILE__ ) . 'build/index.asset.php');

	wp_register_script(
		'contodo-components-collaborator-card',
		plugins_url( 'build/index.js', __FILE__ ),
		$asset_file['dependencies'],
		$asset_file['version']
	);

	wp_register_style(
		'contodo-components-collaborator-card-editor',
		plugins_url( 'editor.css', __FILE__ ),
		array( 'wp-edit-blocks' ),
		filemtime( plugin_dir_path( __FILE__ ) . 'editor.css' )
	);

	wp_register_style(
		'contodo-components-collaborator-card',
		plugins_url( 'style.css', __FILE__ ),
		array( ),
		filemtime( plugin_dir_path( __FILE__ ) . 'style.css' )
	);

	register_block_type( 'contodo-components/collaborator-card', array(
		'style' => 'contodo-components-collaborator-card',
		'editor_style' => 'contodo-components-collaborator-card-editor',
		'editor_script' => 'contodo-components-collaborator-card',
	) );

  if ( function_exists( 'wp_set_script_translations' ) ) {
    /**
     * May be extended to wp_set_script_translations( 'my-handle', 'my-domain',
     * plugin_dir_path( MY_PLUGIN ) . 'languages' ) ). For details see
     * https://make.wordpress.org/core/2018/11/09/new-javascript-i18n-support-in-wordpress/
     */
    wp_set_script_translations( 'contodo-components-collaborator-card', 'contodo-components' );
  }

}
add_action( 'init', 'contodo_components_collaborator_card_register_block' );
