/**
 * External dependencies
 */
import React from 'react';
const path = require( 'path' );

/**
 * Internal dependencies
 */
import '../style.css';
import TwoUp from './two-up.js';

export default { 
	title: 'Components/Two Up', 
	component: TwoUp,
	argTypes: {
		className: {
			control: { 
				type: 'select', 
				options: {
					'image right - dark':'is-style-darkImageRight',
					'image right - light':'is-style-lightImageRight',
					'image left - dark':'is-style-darkImageLeft',
					'image left - light':'is-style-lightImageLeft',
				},
				disable: true,
			},
		},
		imageWidth: {
			control: {
				type: 'select',
				options: [25, 33, 42, 50, 58 ,67, 75],
			}
		},
		children: {
			control: {
				type: 'select',
				options: {
					'bag image': 'https://heavyish.com/sites/default/files/styles/rollover_large/public/IGP0917.JPG',
					'people image': 'https://images.pexels.com/photos/3228704/pexels-photo-3228704.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260',
					'camping image': 'https://images.pexels.com/photos/1687845/pexels-photo-1687845.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260',
				},
			}
		}
	},
	args: {
		className: 'is-style-lightImageRight',
	  subtitle: 'Category',
	  title: 'Thoughtful and Engaging',
	  linkText: 'Consumer Solace',
	  url: 'https://heavyish.com/stuff/basket-case/',
	  absLink: '/stuff/basket-case/',
	  content: '<p>I never expected to read such an incredible example of ipsum.  It left me feeling like I could have done so much more with my life.  Was it too late?  Could I pick up the pieces?</p>',
	  imageWidth: 50,
	  children: 'https://heavyish.com/sites/default/files/styles/rollover_large/public/IGP0917.JPG',
	},
}

const Template = (args) => (
	<TwoUp {...args}>
		<figure>
			<img src={ args.children } alt='example image' />
		</figure>
	</TwoUp>
);

export const ImageRightLight = Template.bind({});

export const ImageRightDark = Template.bind({});
ImageRightDark.args = {
	className: 'is-style-darkImageRight',
}

export const ImageLeftLight = Template.bind({});
ImageLeftLight.args = {
	className: 'is-style-lightImageLeft',
}

export const ImageLeftDark = Template.bind({});
ImageLeftDark.args = {
	className: 'is-style-darkImageLeft',
}
