import classnames from 'classnames';

import { RichText } from '@wordpress/block-editor';
import { forwardRef } from '@wordpress/element';

const Subtitle = ( props ) => {
	if (props.value && props.value.length > 0) 
		return <RichText.Content tagName="h5" value={ props.value } />
}
const Title = ( props ) => {
	if (props.value && props.value.length > 0) 
		return <RichText.Content tagName="h2" value={ props.value } />
}
const Content = ( props ) => {
	if (props.value && props.value.length > 0) 
		return <RichText.Content tagName="div" className="description" value={ props.value } />
}
const Link = ( props ) => {
	if (props.url && props.url.length > 0 && props.text && props.text.length > 0) 
		return (
			<div><a href={ props.url }>{ props.text }</a></div>
		)
}

export function TwoUp( props, ref ) {
  const {
		className,
    subtitle,
    title,
    linkText,
    url,
    absLink,
    content,
    imageWidth,
    children,
    ...additionalProps
	} = props;

	const classes = classnames( 'contodo', 'two-up', 'alignfull', className, {
		[`image-${imageWidth}`]: true,
	} );

  return (
    <div className={ classes }>
			<div className={ "image" }>
				{ children }
			</div>
			<div className={ "content" }>
				<Subtitle value={ subtitle } />
				<Title value={ title } />
				<Content value={ content } />
				<Link text={ linkText } url={ absLink } />
			</div>
		</div>
  );
}

export default forwardRef( TwoUp );